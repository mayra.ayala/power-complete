
//Validacion de datos input

const seriales = document.getElementById('inputserial')
const ubicaciones = document.getElementById('myinput1')
const defectos = document.getElementById('myinput2')

var error = document.getElementById('error')
error.style.color = 'black'

function datavalidation(){
    var mensajesError = []

    if(seriales.value === null || seriales.value === ''){
        mensajesError.push('Data error.  Ingresa serial')
    }
    if(ubicaciones.value === null || ubicaciones.value === ''){
        mensajesError.push('ingresa ubicacion')
    }
    if(defectos.value === null || defectos.value === ''){
        mensajesError.push('ingresa defecto')
    }else{
        enviarform()
    }
    error.innerHTML=mensajesError.join(',')
}


function success(){
    Swal.fire({
        position: "top-end",
        icon: "success",
        title: "New defect has been saved",
        showConfirmButton: false,
        timer: 1500
      });
}

function errorcontraseña(){
    Swal.fire({
        icon: "error",
        title: "Error Password ",
        text: "Try again!",
        //footer: '<a href="#">Why do I have this issue?</a>'
      });
}