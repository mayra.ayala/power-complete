/***Proyecto power 
** Inspecion en unidades con defectos 'x'
*/
/** CARGAR MODELOS AL CARGAR LA PAGINA */

let ubicacion
let defecto
let serial
let excesodesoldadura
let componentedanado
let evidenciadepin 
let bolsoldadura
let valor
let contraseña
let adddefects
// Declaracion de canvas 
let canvasprincipal = document.getElementById('canvasimagenF') //canvas 1,3
let canvasprincipalctx = canvasprincipal.getContext('2d')
//let angulo = 0

/*function contorn(){
    canvasprincipalctx.clearRect(0, 0, canvasprincipal.width, canvasprincipal.height)
    canvasprincipalctx.strokeStyle = "hsl(" + angulo + ", 100%, 50%)"; 
    canvasprincipalctx.lineWidth = 5;
    canvasprincipalctx.strokeRect(10, 10, 100, 100)
    angulo = (angulo + 1 ) % 360
    requestAnimationFrame(contorn)
}
contorn()*/
//Cargar el valor almacenado del storage.getItem
window.onload = function() {
    //function calis(){  
    //var valorAlmacenado = localStorage.getItem("valores");
    let arrayAlmacenado = JSON.parse(localStorage.getItem('valores')) || [];
    console.log("Valores Items",arrayAlmacenado)
    var select = document.getElementById("myinput1");
    console.log("datos en el select:",select)
    arrayAlmacenado.forEach(function(valor){
        var option = document.createElement('option');
        option.text = valor;
        option.value = valor;
        select.add(option)
    }) 
}

function deletelementstorage(){
    var arrayAlmacenado2 = JSON.parse(localStorage.getItem('valores')) || [];

    var indiceAEliminar = arrayAlmacenado2.indexOf(1);
    console.log(indiceAEliminar)
    if(indiceAEliminar !== -1){
        arrayAlmacenado2.splice(indiceAEliminar,1)
    }
    localStorage.setItem('valores', JSON.stringify(arrayAlmacenado2))
}

//*********** Borrar contenido del array almacenado en localstorage */
function deletelocal(){
    localStorage.removeItem('valores')
}
//----------------------------------------------------------- Secuencia Segmentacion -------------------------------//
async function Segmentacion(){
    await open_cam()
    await captureimage()
    canbughide()
    //colocar()
    await loadmodel() //Cargando primer modelo de red neuronal
    //await predict()
    //recorrerpoint(1)

}

//** Funciones principales */
async function colocar(){
    return new Promise(async resolve => {
        canvasprincipalctx.drawImage(fullimage, 0, 0, 1920, 1080, 0, 0, 900, 500)
        resolve('resolved')
    })
} 

async function recorrerpoint(point){
    switch(point){
        case 1: 
                await predict(fullimage)
                console.log("Estoy en punto 1 ")
        break
        case 2:
                console.log("Estoy en punto 2 ")
        break
    }
}
//----------------------------------------------------------- Funciones camaras

async function open_cam(point) {// Resolve de 2 segundos
//Cnfiguracion abiri camaras por punto con ID
    return new Promise(async resolve => {
         let camid 
         camid = "b018bdec8ce28e5652c262616b9fd067e9144dacf6c13ded795774553e2e7478"
         /*if(point ==1) {camid="a96e34008b46a256d73fe6a76834190494f68d3b297008ab0d9d1e64eaa19349", console.log("Entre a camara 1")}
         await pause()
         if(point ==2) {camid="385e70ed0dccdec061905056fb14a68758b2c0f463a1541213c93aee0bc1f9ed", console.log("Entre a camara 2")}
         if(point ==3) {camid="a7cf8b72dd8baa2199a8a8dc32076da98c85fe65912f04796416f0361bd48c0c", console.log("Entre a camara 3")}*/
        // camara UR

        const video = document.querySelector('video')
        const vgaConstraints = {
            video:
            {
                width: { ideal: 1920 },
                height: { ideal: 1080 },
                deviceId: camid
            }
        }
        await navigator.mediaDevices.getUserMedia(vgaConstraints).then((stream) => { video.srcObject = stream }).catch(function (err) { console.log(err.name) })
        setTimeout(function fire() { resolve('resolved'); }, 2000) //tiempo para el opencam
    })//Cierra Promise principal
}

function captureimage() {// Resolve de 2 segundos
    return new Promise(async resolve => {
        const video = document.getElementById('video')

        fullimagectx.drawImage(video,0,0, fullimage.width, fullimage.height)
        canvasprincipalctx.drawImage(fullimage,0,0, canvasprincipal.width,canvasprincipal.height)
        setTimeout(function fire() {resolve('resolved')},1000 )
           /* let image = document.getElementById('recorte');
            let contexim2 = image.getContext('2d');
    
            var video = document.getElementById("video");
    
            w = image.width;
            h = image.height;
    
            contexim2.drawImage(video, 0, 0, image.width, image.height);
            //var dataURI = canvas.toDataURL('image/jpeg');
            //setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
           */
            resolve('resolved')
        });
}

function mapcams() {//Mapeo de camaras 
    navigator.mediaDevices.enumerateDevices()
        .then(devices => {
            const filtered = devices.filter(device => device.kind === 'videoinput');
            console.log('Cameras found', filtered.length);
        });
}

function stopcam() {//Paro de camaras
    return new Promise(async resolve => {
        const video = document.querySelector('video');
        // A video's MediaStream object is available through its srcObject attribute
        const mediaStream = video.srcObject;
        // Through the MediaStream, you can get the MediaStreamTracks with getTracks():
        const tracks = mediaStream.getTracks();
        tracks.forEach(track => { track.stop() })//;console.log(track);
        setTimeout(function fire() { resolve('resolved'); }, 500);
    });//Cierra Promise principal
}

function snapshot(){//Procesamiento para captura de camaras 
    return new Promise(async resolve =>{
        var dataURI = fullimage.toDataURL('image/jpeg');
        savepicfalla(dataURI,serial,valor,ubicacion); //funcion para mandar datos a backend 
        //console.log("Pic Sent--"+sn+"--"+point);
        //setTimeout(function fire(){resolve('resolved');},2000);//Temporal para programacion de secuencia
        resolve('resolved')});
}

//-------------------------------------------------------Funciones debug

function canbughide(){
    return new Promise(async resolve => {
    document.getElementById('CanvasFHD').style.visibility = "hidden"
    //document.getElementById('miVideoContainer').style.visibility = "hidden"
    document.getElementById('video').style.visibility = "hidden"
    resolve('resolved')});
}

function canbugshow(){
    return new Promise(async resolve => {
    // Ejemplo 
    //document.getElementById( 'CanvasFHD' ).style.visibility = "visible"
    document.getElementById('CanvasFHD').style.visibility = "visible"
    document.getElementById('miVideoContainer').style.visibility = "visible"
    document.getElementById('video').style.visibility = "visible"
    resolve('resolved')});
}

async function pause() {
    return new Promise(async resolve => {
        setTimeout(function pausea() { resolve('resolved') }, 3000)
    });
}

//--------------------------------------------- Boton Agregar defecto y valores de inputs -----------------------------------------//
function agregardefecto(){   
    if(document.getElementById('mydivform').style.display = "block"){
        document.getElementById('mydivform').style.display = "block"
        console.log("Estoy dentro de funcion agregar defecto")
    }else{
        document.getElementById('mydivform').style.display = "none"   
    }
}

//Esconder Div de agregar defecto
function esconderdiv(){
    if(document.getElementById('mydivform').style.display = "none"){
        document.getElementById('mydivform').style.display = "none"
        console.log("Estoy dentro de funcion agregar defecto")
    }else{
        document.getElementById('mydivform').style.display = "block"   
    }
}

function valorserial(){ // funcion que guarda el elemento de input 
    return new Promise(async resolve =>{ 
    serial = document.getElementById('inputserial').value // Se almacena en una variable
    console.log(serial)
    resolve('resolved')})
}

//Datos de boton enviar datos en ubicacion
function valorinput1(select){ // funcion que guarda el elemento de input 
    return new Promise(async resolve =>{ 
        //var select = document.getElementById('myinput1')
       // var value = select.options[select.selectedIndex].value
         valor = select.value
        console.log("Soy valor", valor)
        if(valor == "Exceso de soldadura"){
             excesodesoldadura = valor
            console.log("Se cumplio el if",excesodesoldadura)
        }else if(valor == "Componente Danado"){
             componentedanado = valor
            console.log("Opcion 2",componentedanado)
        }else if(valor == "Evidencia de Pin"){
            evidenciadepin = valor
            console.log("Opcion 3",evidenciadepin)
        }else if(valor == "Bolitas de soldadura"){
            bolsoldadura = valor
            console.log("Opcion 4", bolsoldadura)
        }else{
            console.log("No existe ninguna")
        }

    resolve('resolved')})
}

function valorinput2(){ // funcion que guarda el elemento de input 
    return new Promise(async resolve =>{ 
    ubicacion = document.getElementById('myinput2').value // Se almacena en una variable
    console.log(ubicacion)
    resolve('resolved')})
}

function nuevodefecto(){//Guarda el valor del input en el div3
    return new Promise(async resolve =>{ 
        adddefects = document.getElementById('inputdiv3').value // Se almacena en una variable
        console.log(adddefects)
        resolve('resolved')})
}
/********************************* Segundo formulario contraseña */
function valorcontraseña(){//Guarda el valor ingresado en el input contraseña
    contraseña = document.getElementById('newinput').value
    console.log("Contraseña: ",contraseña)
}

function guardarcontra(){
    if(contraseña == "hola"){
        console.log("Contraseña correcta")
        //Si la contraseña es correcta escondera el dif de contraseña
            if(document.getElementById('newdiv').style.display = "none"){
                document.getElementById('newdiv').style.display = "none"
            }else{
                document.getElementById('newdiv').style.display = "block"   
            }
        //mostrar div de crear contraseña
                if(document.getElementById('divagregar').style.display = "block"){
                    document.getElementById('divagregar').style.display = "block"
                }else{
                    document.getElementById('divagregar').style.display = "none"   
                }
    }else{
        errorcontraseña()
        //alert("Contraseña Incorrecta")
        console.log("Contraseña incorrecta")
    }
}

/***************************** Agregar nuevo valor al input */
async function crearnuevodefecto(){
    
    const nuevoValorInput = document.getElementById('inputdiv3');
    const miSelect = document.getElementById('myinput1');

    // Agrega el nuevo valor como opción al select
    const nuevoOption = document.createElement('option');
    nuevoOption.text = nuevoValorInput.value;
    miSelect.add(nuevoOption);

    // Almacena el nuevo valor en localStorage
    const valoresGuardados = JSON.parse(localStorage.getItem('valores')) || [];
    valoresGuardados.push(nuevoValorInput.value);
    localStorage.setItem('valores', JSON.stringify(valoresGuardados));

// Limpia el campo de entrada
        nuevoValorInput.value = '';
        esconderdiv4()
        verdiv1()
        success()
        
}

function cargarValoresGuardados() {
    const miSelect = document.getElementById('myinput1');
    const valoresGuardados = JSON.parse(localStorage.getItem('valores')) || [];
    
    // Agrega cada valor almacenado como opción al select
    for (const valor of valoresGuardados) {
    const nuevoOption = document.createElement('option');
    nuevoOption.text = valor;
    miSelect.add(nuevoOption);
    }
}
/*********************Esconder y mostrar divs */
function esconderdiv3(){
    return new Promise(async resolve => {
    if(document.getElementById('newdiv').style.display = "block"){
        document.getElementById('newdiv').style.display = "block"
    }else{
        document.getElementById('newdiv').style.display = "none"   
    }
    resolve('resolved')});
}

function verdiv1(){
    return new Promise(async resolve => {
    if(document.getElementById('mydivform').style.display = "none"){
        document.getElementById('mydivform').style.display = "none"
    }else{
        document.getElementById('mydivform').style.display = "block"   
    }
    resolve('resolved')});
}

function esconderdiv4(){
    return new Promise(async resolve => {
    if(document.getElementById('divagregar').style.display = "none"){
        document.getElementById('divagregar').style.display = "none"
    }else{
        document.getElementById('divagregar').style.display = "block"   
    }
    resolve('resolved')});
}

/**** validacion Boton de enviar datos */
function habilitar(){
    return new Promise(async resolve =>{
        var btndisabled = document.getElementById('btnagrega')
        btndisabled.disabled = false
        console.log("Estoy habilitado")
        resolve('resolved')})
}

function deshabilitar(){
    return new Promise(async resolve =>{
    var btndisabled = document.getElementById('btnagrega')
    btndisabled.disabled = true
    console.log("Estoy deshabilitado")
    resolve('resolved')}) 
}


//******************************************** Validacion de datos "Agregar defecto" */
async function enviarform(){
    console.log("Boton funciona..")
    habilitar()
    await open_cam()
    await captureimage()
    canbughide()
    snapshot()

   // setTimeout(function fire(){location.reload()},4500);// temporizador para limpiar pantalla
}

/********************************************* Boton agregar nuevo defecto ****/
function newdefect(){
    if(document.getElementById('newdiv').style.display = "block"){
        document.getElementById('newdiv').style.display = "block"
        console.log("Estoy dentro de funcion agregar defecto")
    }else{
        document.getElementById('newdiv').style.display = "none"   
    }
    esconderdiv()//Esconde el div de agregar defecto
}

/********************************************* Guardado de imagenes con defecto */

function savepicfalla(uri,serial,valor,ubicacion){   
    console.log("Datos obtenidos:",serial, valor,ubicacion)
   //Valor es el defecto 
    let fusion = serial+"-"+ubicacion
    console.log("Datos: ",valor,fusion)
    const socket = io();
    socket.emit('picsaving',uri,valor,fusion);	
}  


 //*********************************************** IA  *******************************/ 
/*** Presencia ausencia deteccion de objetos */

let model = new cvstfjs.ObjectDetectionModel();

async function loadmodel(){

    await model.loadModelAsync('../neural-network2/model.json');
    console.log("Cargando modelo...")
    console.log(model)
    await predict()
}

async function predict(fullimage){
     fullimage = document.getElementById( 'CanvasFHD' )
    let input_size = model.input_size

    // Take & Pre-process the image
	let image = tf.browser.fromPixels(fullimage, 3) // se utiliza para crear un tensor de valores de píxeles de una imagen específica. 
	image = tf.image.resizeBilinear(image.expandDims(),[input_size, input_size]) //  
	let result = await model.executeAsync(image)
	console.log("Soy result: ",result)
    console.log(input_size)

    console.log(result[0])
    let firstElement = result[0][0]
    console.log(firstElement)//Primer elemento del array de arrays 
    //let predictions = result[1][0]
    //console.log("Soy predictions", predictions)//Primer valor de porcentaje de confiabilidad 
    let etiqueta = result[2][0]
    console.log(etiqueta)//Ubicacion de primer etiqueta 
    //await results(predictions,result,etiqueta)
    let score = result[1][0] // Probabilidad mas alta del array 
    console.log('this is real score: ', score)
    let predictions = {
        bbox: firstElement,//Coordenadas de busqueda de acuerdo a entrenamiento
        class: "test",//Nombre del objeto
        score: score
    }
    await predictWebcam(predictions)
}

async function results(predictions,result,etiqueta){
    if (predictions > 0.29) { 
        console.log("falle: "+predictions)
        let videoElement = document.getElementById("video")
        let nuevoParrafo = document.createElement("p")
        nuevoParrafo.innerText = clases[result[2][0]] + ': '
            + Math.round(parseFloat(predictions) * 100)
            + '%';
           // console.log(p)
           miVideoContainer.appendChild(nuevoParrafo)
    }else if(predictions < 0.29){
        console.log("menor al valor ")
        let videoElement = document.getElementById("video")
        let nuevoParrafo = document.createElement("p")
        nuevoParrafo.innerText = clases[result[2][0]] + ': '
            + Math.round(parseFloat(predictions) * 100)
            + '%';
            //console.log(p)
            miVideoContainer.appendChild(nuevoParrafo)
    }

}

var children = [];
// Placeholder function for next step. 
async function predictWebcam(predictions) { // funcion que contiene el proceso del reconocimento 
    console.log("Estoy en funcion predictwebcam")
    console.log(predictions)
    // Now let's start classifying a frame in the stream.      // Empezar a clasificar un cuadro en la secuencia 
    //model.detect(video).then(function (predictions) { 
    // Remove any highlighting we did previous frame.     // Elimine cualquier resaltado que hicimos en el cuadro anterior.
   /*for (let i = 0; i < children.length; i++) {
    liveView.removeChild(children[i]);
  }
  children.splice(0); //cambia el contenido de un array eliminando elementos existentes y/o agregando nuevos elementos
  */
  // Now lets loop through predictions and draw them to the live view if
  // they have a high confidence score.
  //for (let n = 0; n < predictions.length; n++) {
    // If we are over 66% sure we are sure we classified it right, draw it!
    if (predictions.score > 0.01) {
        //let videoElement = document.getElementById("canvasimagenF")
        let nuevoParrafo = document.createElement('p')
      //const p = document.createElement('p');
      nuevoParrafo.innerText= predictions.class  + ' - with ' 
          + Math.round(parseFloat(predictions.score) * 100) 
          + '% confidence.';
      nuevoParrafo.style = 'margin-left: '
          + predictions.bbox[0] * 900 + 'px; margin-top: ' //640
          + (predictions.bbox[1] * 500 ) + 'px; width: '  //480
          + (predictions.bbox[2] * 900) + 'px; top: 0; left: 0;'; //640
          console.log(nuevoParrafo.style)
      const highlighter = document.createElement('div');
      highlighter.setAttribute('class', 'highlighter');
      highlighter.style = 'left: '
          + predictions.bbox[0] * 900 + 'px; top: ' //640
          + predictions.bbox[1] * 500 + 'px; width: '  //480
          + predictions.bbox[2] * 900 + 'px; height: ' //640
          + predictions.bbox[3] * 500 + 'px;'; // 480
        console.log(highlighter.style)
          miVideoContainer.appendChild(highlighter);
          miVideoContainer.appendChild(nuevoParrafo);
      children.push(highlighter);
      children.push(nuevoParrafo);
    }
  //}
    //Call this function again to keep predicting when the browser is ready.
   //window.requestAnimationFrame(predictWebcam);
}

//LImpiar ventana de busqueda en el video 
async function removeHighlights() {
	for (let i = 0; i < children.length; i++) {
		miVideoContainer.removeChild(children[i])
	}
	children = []
}

